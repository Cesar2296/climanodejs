const lugar=require('./lugar/lugar');
const clima= require('./clima/clima');
const argv=require('yargs').options({
    direccion:{
        alias:'d',
        desc:'Para obtener direccion',
        demand:true
    }
}).argv;

const getInfo=async(direccion)=>{
    try {
        const coordenadas= await lugar.getLugarLatLng(direccion);
        const temperatura=await clima.getClima(coordenadas.lat,coordenadas.lng);
        const resp=`La temperatura de ${coordenadas.direccion} es de ${temperatura}`;
        return resp;
    } catch (error) {
        return `No se pudo determinar la temperatura de ${direccion}`;
    }
}
getInfo(argv.direccion).then(console.log);



