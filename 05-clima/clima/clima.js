const axios=require('axios');
const apikey='773a38f04e8b221f690bfaace7fbaf77';
const units='metric';

const getClima=async(lat,lng)=>{
    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=${apikey}&units=${units}`);
    if(resp.data.main.temp===0){
        throw new Error(`No hay resultados para ${lat},${lng}`);
    }
    return resp.data.main.temp;
}
module.exports={
    getClima
}