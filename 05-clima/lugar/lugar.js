const axios=require('axios');
const getLugarLatLng=async(dir)=>{
    const encodeUrl=encodeURI(dir);
    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodeUrl}`,
        timeout: 1000,
        headers: {'X-RapidAPI-Key': 'cbd97e415emshce0d0e42914176ap1fca40jsn2c46344f4eaa'}
      });
    const res=await instance.get();
    if(res.data.Results.length===0){
        throw new Error(`No hay resultados para ${dir}`);
    }
    const data=res.data.Results[0];
    const direccion=data.name;
    const lat=data.lat;
    const lng=data.lon;

    return{
        direccion,
        lat,
        lng
    }       
}
module.exports={
    getLugarLatLng
}